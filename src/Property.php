<?php

namespace OpenapiNextGeneration\OpenapiRoutesMapperPhp;

class Property implements \JsonSerializable
{
    protected $sampleData;
    protected $skipReadOnlyProperties;
    protected $skipWriteOnlyProperties;


    public function __construct(
        array $specification,
        bool $skipReadOnlyProperties = false,
        bool $skipWriteOnlyProperties = false
    )
    {
        $this->skipReadOnlyProperties = $skipReadOnlyProperties;
        $this->skipWriteOnlyProperties = $skipWriteOnlyProperties;

        if (isset($specification['example'])) {
            $this->sampleData = $specification['example'];
        } else {
            $this->sampleData = $this->createExampleFromStructure($specification);
        }
    }

    public function jsonSerialize()
    {
        return $this->sampleData;
    }

    protected function createExampleFromStructure(array $specification)
    {
        $type = $specification['type'] ?? '';
        switch ($type) {
            case 'object':
                $sampleData = new \stdClass();
                foreach ($specification['properties'] ?? [] as $propertyName => $propertySpecification) {
                    if (
                        ($this->skipReadOnlyProperties && ($propertySpecification['readOnly'] ?? false))
                        || ($this->skipWriteOnlyProperties && ($propertySpecification['writeOnly'] ?? false))
                    ) {
                        continue;
                    }

                    $sampleData->$propertyName = new Property($propertySpecification);
                }
                break;
            case 'array':
                $sampleData = [
                    new Property($specification['items'] ?? [])
                ];
                break;
            case 'string':
                $typeDefault = 'default';
                break;
            case 'integer':
                $typeDefault = 1;
                break;
            case 'number':
                $typeDefault = 1.2;
                break;
            case 'boolean':
                $typeDefault = true;
                break;
            default:
                $sampleData = '';
                break;
        }

        if (isset($typeDefault)) {
            $sampleData = $specification['default'] ?? $specification['enum'][0] ?? $typeDefault;
        }

        return $sampleData;
    }
}