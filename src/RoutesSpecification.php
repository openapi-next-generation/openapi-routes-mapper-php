<?php

namespace OpenapiNextGeneration\OpenapiRoutesMapperPhp;

class RoutesSpecification
{
    protected $tags = [];

    public function __construct(array $specification)
    {
        foreach ($specification['tags'] ?? [] as $tagSpecification) {
            $tagName = $tagSpecification['name'] ?? 'default';
            $this->tags[$tagName] = [];
        }

        foreach ($specification['paths'] ?? [] as $pathString => $pathSpecification) {
            foreach ($pathSpecification as $method => $action) {
                $route = new Route($method, $pathString, $action);

                foreach ($route->getTags() as $methodTag) {
                    if (isset($this->tags[$methodTag])) {
                        $this->tags[$methodTag][] = $route;
                    }
                }
            }
        }
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function getUniqueRoutes(): array
    {
        $uniqueRoutes = [];

        foreach ($this->tags as $methods) {
            /* @var $route Route */
            foreach ($methods as $route) {
                $uniqueRoutes[] = $route;
            }
        }

        return $uniqueRoutes;
    }
}