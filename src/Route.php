<?php

namespace OpenapiNextGeneration\OpenapiRoutesMapperPhp;

class Route
{
    protected $httpMethod;
    protected $path;
    protected $methodSpecification;
    protected $tags;

    public function __construct(string $httpMethod, string $path, array $methodSpecification)
    {
        $this->httpMethod = strtoupper($httpMethod);
        $this->path = $path;
        $this->methodSpecification = $methodSpecification;
        $this->tags = $methodSpecification['tags'] ?? [];
    }

    public function getHttpMethod(): string
    {
        return $this->httpMethod;
    }

    public function getActionName(): string
    {
        return strtolower($this->httpMethod) . $this->getParamBasedActionSuffix();
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getMethodSpecification(): array
    {
        return $this->methodSpecification;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * Concat all path parts
     */
    public function getPathName(bool $includeParams = true): string
    {
        $validLiterals = [];
        foreach (explode('/', $this->path) as $segment) {
            if (
                empty($segment)
                || (!$includeParams && preg_match('/^\{.*\}$/', $segment))
            ) {
                continue;   //skip any {route-params}
            }
            $validLiterals[] = str_replace(['{', '}'], '', $segment);
        }
        return implode('/', $validLiterals);
    }

    /**
     * Get a unique identifier for the resource (all methods of the same resource will have the same result)
     */
    public function getUniqueResourceName(): string
    {
        $rawName = ucfirst($this->getPathName(false));
        return str_replace(['-', '_'], '', ucwords($rawName, '-_'));
    }

    /**
     * Build an example path filling route parameters with example values and adding all query parameters
     *
     * @return string
     */
    public function buildExamplePath(): string
    {
        $pathParameters = [];
        $queryParameters = [];
        foreach ($this->methodSpecification['parameters'] ?? [] as $parameter) {
            $value = $parameter['schema']['example'] ?? $parameter['schema']['enum'][0]
                ?? $parameter['schema']['default'] ?? '1';
            if (isset($parameter['in']) && $parameter['in'] === 'path') {
                $pathParameters[$parameter['name']] = $value;
            } elseif (isset($parameter['in']) && $parameter['in'] === 'query') {
                $queryParameters[$parameter['name']] = $value;
            }
        }

        $path = preg_replace_callback(
            '/{.+}/U',
            function ($match) use ($pathParameters) {
                return $pathParameters[trim($match[0], '{}')] ?? '1';
            },
            $this->path
        );
        if (!empty($queryParameters)) {
            $path .= '?' . http_build_query($queryParameters);
        }

        return $path;
    }

    /**
     * Build an example request body from specifications example, default and enum values
     *
     * @return string
     */
    public function buildExampleRequestBody(): ?Property
    {
        if (isset($this->methodSpecification['requestBody']['content']['application/json']['schema'])) {
            return new Property(
                $this->methodSpecification['requestBody']['content']['application/json']['schema'],
                true
            );
        } else {
            return null;
        }
    }

    /**
     * Build the path for a request validating schema.json file for this route
     *
     * @return string
     */
    public function buildRequestJsonSchemaFilePath(): string
    {
        $pathName = $this->getPathName();
        if (!empty($pathName)) {
            $pathName .= '/';
        }
        return $pathName . strtolower($this->getHttpMethod()) . '/request/schema.json';
    }

    /**
     * Build the path for a response validating schema.json file for this route
     *
     * if no $httpStatusCode is given it will take the 200ishest which exists
     *
     * @param int|null $httpStatusCode
     * @return string
     */
    public function buildResponseJsonSchemaFilePath(?int $httpStatusCode = null): string
    {
        $pathName = $this->getPathName();
        if (!empty($pathName)) {
            $pathName .= '/';
        }

        if ($httpStatusCode === null && isset($this->methodSpecification['responses'])) {
            $responses = array_keys($this->methodSpecification['responses']);
            sort($responses);
            $httpStatusCode = 200;
            foreach ($responses as $code) {
                if ($code >= 200) {
                    $httpStatusCode = $code;
                    break;
                }
            }
        }

        return $pathName . strtolower($this->getHttpMethod()) . '/response/' . $httpStatusCode . '/schema.json';
    }

    protected function getParamBasedActionSuffix(): string
    {
        $validParams = [];
        foreach (explode('/', $this->path) as $segment) {
            if (
                empty($segment)
                || (!preg_match('/^\{.*\}$/', $segment))
            ) {
                continue;   //skip anything else than {route-params}
            }
            $validParams[] = str_replace(['{', '}'], '', $segment);
        }

        if (count($validParams) === 0) {
            return '';
        } else {
            $params = implode('_And', $validParams);
            return 'By' . ucfirst(str_replace(['-', '_'], '', ucwords($params, '-_')));
        }
    }
}
